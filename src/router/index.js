import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: HomeView,
    redirect: '/sys-admin/index', // 重定向
    children: [
      // ====================== 以下是【首页】的路由配置 ======================
      {
        path: '/sys-admin/index',
        component: () => import('../views/sys-admin/SystemAdminIndex.vue')
      },
      // ====================== 以下是【商品管理】的路由配置 ======================
      {
        path: '/sys-admin/product/spu-management',
        component: () => import('../views/sys-admin/product/SpuManagement.vue')
      },
      {
        path: '/sys-admin/product/spu-management/sku-list',
        component: () => import('../views/sys-admin/product/SkuListView.vue')
      },
      {
        path: '/sys-admin/product/spu/add-new-1',
        component: () => import('../views/sys-admin/product/SpuAddNewView1.vue')
      },
      {
        path: '/sys-admin/product/spu/add-new-2',
        component: () => import('../views/sys-admin/product/SpuAddNewView2.vue')
      },
      {
        path: '/sys-admin/product/spu/add-new-3',
        component: () => import('../views/sys-admin/product/SpuAddNewView3.vue')
      },
      {
        path: '/sys-admin/product/spu/add-new-4',
        component: () => import('../views/sys-admin/product/SpuAddNewView4.vue')
      },
      {
        path: '/sys-admin/product/album',
        component: () => import('../views/sys-admin/product/AlbumListView.vue')
      },
      {
        path: '/sys-admin/product/album/add-new',
        component: () => import('../views/sys-admin/product/AlbumAddNewView.vue')
      },
      {
        path: '/sys-admin/product/album/picture-list',
        component: () => import('../views/sys-admin/product/AlbumPictureListView.vue')
      },
      {
        path: '/sys-admin/product/category',
        component: () => import('../views/sys-admin/product/CategoryListView.vue')
      },
      {
        path: '/sys-admin/product/category/add-new',
        component: () => import('../views/sys-admin/product/CategoryAddNewView.vue')
      },
      {
        path: '/sys-admin/product/attribute-template',
        component: () => import('../views/sys-admin/product/AttributeTemplateListView.vue')
      },
      {
        path: '/sys-admin/product/attribute-template/add-new',
        component: () => import('../views/sys-admin/product/AttributeTemplateAddNewView.vue')
      },
      {
        path: '/sys-admin/product/attribute',
        component: () => import('../views/sys-admin/product/AttributeListView.vue')
      },
      {
        path: '/sys-admin/product/attribute/add-new',
        component: () => import('../views/sys-admin/product/AttributeAddNewView.vue')
      },
      {
        path: '/sys-admin/product/brand',
        component: () => import('../views/sys-admin/product/BrandListView.vue')
      },
      {
        path: '/sys-admin/product/brand/add-new',
        component: () => import('../views/sys-admin/product/BrandAddNewView.vue')
      },
      // ====================== 以下是【权限管理】的路由配置 ======================
      {
        path: '/sys-admin/permission/admin',
        component: () => import('../views/sys-admin/permission/AdminListView.vue')
      },
      {
        path: '/sys-admin/permission/admin/add-new',
        component: () => import('../views/sys-admin/permission/AdminAddNewView.vue')
      },
    ]
  },
  {
    path: '/login',
    component: () => import('../views/LoginView.vue')
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
